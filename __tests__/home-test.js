import 'react-native';
import React from 'react';
import Home from '../src/screens/Home';
import renderer from 'react-test-renderer';

describe('Home', () => {
  test('Home Snapshot', () => {
    const snap = renderer.create(<Home />).toJSON();
    expect(snap).toMatchSnapshot();
  });
});
