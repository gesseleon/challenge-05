import 'react-native';
import React from 'react';
import renderer, {create} from 'react-test-renderer';
import Login from '../src/screens/login';

describe('Login', () => {
  test('Login Page Snapshot', () => {
    const snap = renderer.create(<Login />).toJSON();
    expect(snap).toMatchSnapshot();
  });

  it('function and state test care', () => {
    let HomeData = renderer.create(<Login />).getInstance();

    HomeData.onSend.body();

    expect(HomeData.onSend.body).toEqual('');
  });
});
